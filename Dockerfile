FROM openjdk:17
ADD /target/Factorial-0.0.1-SNAPSHOT.jar factorial.jar
ENTRYPOINT ["java", "-jar", "factorial.jar"]