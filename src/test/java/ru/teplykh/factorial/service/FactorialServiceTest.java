package ru.teplykh.factorial.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import ru.teplykh.factorial.exception.ArgumentNotValidException;
import ru.teplykh.factorial.model.dto.FactorialDto;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class FactorialServiceTest {

    @Value("${max}")
    private int max;

    @Test
    void factorial_ValidArgument_FactorialDto() {
        FactorialService factorialService = new FactorialService();
        factorialService.setMax(max);

        assertEquals(new FactorialDto(BigInteger.valueOf(1)), factorialService.factorial(0));
        assertEquals(new FactorialDto(BigInteger.valueOf(1)), factorialService.factorial(1));
        assertEquals(new FactorialDto(BigInteger.valueOf(120)), factorialService.factorial(5));
        assertEquals(new FactorialDto(BigInteger.valueOf(3628800)), factorialService.factorial(10));
    }

    @Test
    void factorial_ArgumentLessThanZero_ArgumentNotValidException() {
        FactorialService factorialService = new FactorialService();
        factorialService.setMax(max);

        Assertions.assertThrows(ArgumentNotValidException.class, () -> {
            factorialService.factorial(-2);
        });
    }

    @Test
    void factorial_ArgumentGreaterThanMax_ArgumentNotValidException() {
        FactorialService factorialService = new FactorialService();
        factorialService.setMax(max);

        Assertions.assertThrows(ArgumentNotValidException.class, () -> {
            factorialService.factorial(max + 1);
        });
    }
}