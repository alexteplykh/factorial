package ru.teplykh.factorial.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.teplykh.factorial.model.dto.FactorialDto;
import ru.teplykh.factorial.model.dto.RequestDto;
import ru.teplykh.factorial.service.FactorialService;

import java.math.BigInteger;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(FactorialController.class)
class FactorialControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @MockBean
    FactorialService factorialService;

    @Test
    void factorial_GoodRequest_FactorialDto() throws Exception {
        RequestDto request = new RequestDto().setFactorialNum(5);
        FactorialDto response = new FactorialDto(BigInteger.valueOf(120));

        when(factorialService.factorial(5)).thenReturn(response);

        mockMvc.perform(get("/factorial")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(om.writeValueAsString(response)));
    }
}