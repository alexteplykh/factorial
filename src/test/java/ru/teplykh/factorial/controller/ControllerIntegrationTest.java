package ru.teplykh.factorial.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import ru.teplykh.factorial.model.dto.FactorialDto;
import ru.teplykh.factorial.model.dto.ExceptionDto;
import ru.teplykh.factorial.model.dto.RequestDto;
import ru.teplykh.factorial.service.FactorialService;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(printOnlyOnFailure = false)
public class ControllerIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @Autowired
    FactorialService factorialService;

    @Value("${max}")
    private int max;

    @Test
    void factorial_GoodRequest_FactorialDto() throws Exception {
        RequestDto request = new RequestDto().setFactorialNum(5);
        FactorialDto response = new FactorialDto(BigInteger.valueOf(120));

        mockMvc.perform(get("/factorial")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(content().json(om.writeValueAsString(response)));

        FactorialDto factorialActual = factorialService.factorial(request.getFactorialNum());
        assertEquals(BigInteger.valueOf(120), factorialActual.getResult());
    }

    @Test
    void factorial_FactorialNumLessThanZero_ArgumentNotValidException() throws Exception {
        RequestDto request = new RequestDto().setFactorialNum(-1);
        ExceptionDto response = new ExceptionDto("Число должно быть от 0 до " + max);

        mockMvc.perform(get("/factorial")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(om.writeValueAsString(response)));
    }

    @Test
    void factorial_FactorialNumGreaterThanMax_ArgumentNotValidException() throws Exception {
        RequestDto request = new RequestDto().setFactorialNum(max + 1);
        ExceptionDto response = new ExceptionDto("Число должно быть от 0 до " + max);

        mockMvc.perform(get("/factorial")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(om.writeValueAsString(response)));
    }

    @Test
    void factorial_FactorialNumIsNotValid_HttpMessageNotReadableException() throws Exception {
        ExceptionDto response = new ExceptionDto("Введите число");

        mockMvc.perform(get("/factorial")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "factorialNum":text
                                }
                                """))
                .andExpect(status().isBadRequest())
                .andExpect(content().json(om.writeValueAsString(response)));
    }
}
