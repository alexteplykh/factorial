package ru.teplykh.factorial.model.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RequestDto {
    private int factorialNum;
}
