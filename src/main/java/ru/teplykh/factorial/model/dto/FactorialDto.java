package ru.teplykh.factorial.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigInteger;

@AllArgsConstructor
@Data
public class FactorialDto {
    private BigInteger result;
}
