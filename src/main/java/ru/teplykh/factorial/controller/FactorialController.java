package ru.teplykh.factorial.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.teplykh.factorial.model.dto.FactorialDto;
import ru.teplykh.factorial.model.dto.RequestDto;
import ru.teplykh.factorial.service.FactorialService;

@RestController
@RequiredArgsConstructor
public class FactorialController {
    private final FactorialService factorialService;

    @GetMapping("/factorial")
    public FactorialDto getFactorial(@RequestBody RequestDto request) {
        return factorialService.factorial(request.getFactorialNum());
    }
}
