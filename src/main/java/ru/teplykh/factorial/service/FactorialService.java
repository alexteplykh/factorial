package ru.teplykh.factorial.service;

import io.micrometer.core.annotation.Timed;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.teplykh.factorial.model.dto.FactorialDto;
import ru.teplykh.factorial.exception.ArgumentNotValidException;

import java.math.BigInteger;

@Service
@Data
public class FactorialService {

    @Value("${max}")
    private int max;

    @Timed("getFactorial")
    public FactorialDto factorial(int n) {
        if (n < 0 || n > max) {
            throw new ArgumentNotValidException(String.valueOf(max));
        }

        BigInteger factorial = BigInteger.valueOf(1);
        for (int i = 2; i <= n; i++) {
            factorial = factorial.multiply(BigInteger.valueOf(i));
        }

        return new FactorialDto(factorial);
    }
}
