package ru.teplykh.factorial.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.teplykh.factorial.exception.ArgumentNotValidException;
import ru.teplykh.factorial.model.dto.ExceptionDto;

@ControllerAdvice
public class ApiControllerAdvice {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ExceptionDto> handle(HttpMessageNotReadableException e) {
        return ResponseEntity.badRequest().body(new ExceptionDto("Введите число"));
    }

    @ExceptionHandler(ArgumentNotValidException.class)
    public ResponseEntity<ExceptionDto> handle(ArgumentNotValidException e) {
        return ResponseEntity.badRequest().body(new ExceptionDto("Число должно быть от 0 до " + e.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> handle(Exception e) {
        return ResponseEntity.badRequest().body("Что-то пошло не так. Попробуйте еще раз");
    }
}
